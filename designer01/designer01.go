package main

import (
	"log"
	"encoding/gob"
)

type Service interface {
	Start() error
	Stop() error
}

type service struct {
	encoder Encoder
	decoder Decoder
	a       int
	b       string
}
func main() {
	service{encoder:gob.NewDecoder("aa"), decoder:gob.NewDecoder("bb")}
}
func (s service) Start() {
	log.Println("This core service use encoder bug we have choose whick kind encode we use")
	s.decoder.Decode("aaa")
}

func (s service) Stop() {
	log.Println("This core service use encoder bug we have choose whick kind encode we use")
	s.encoder.Encode("aaa")
}

type Encoder interface {
	Encode(message interface{}) error
}
type EncoderFunc func(message interface{}) error

func (f EncoderFunc) Encode(message interface{}) error {
	return f.Encode(message)
}

type Decoder interface {
	Decode(message interface{}) error
}

type DecoderFunc func(message interface{}) error

func (f DecoderFunc) Decode(message interface{}) error {
	return f.Decode(message)
}
