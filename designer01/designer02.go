package main

import "log"

type Worker interface {
	Studyer
	Teacher
}
type Studyer interface {
	study(interface{}) error
}
type StudyFrom interface {
	StudyFrom(Teacher) error
}

type Teacher interface {
	teach(interface{}) error
}
type TeachTo interface {
	TeachTo(Studyer) error
}

type Person struct {
	Name string
	Age  int
}

func Teach(t Teacher, s Studyer) {
	if sf, ok := s.(StudyFrom); ok {
		sf.StudyFrom(t)
	}
	if tt, ok := t.(TeachTo); ok {
		tt.TeachTo(s)
	}

}

func (p Person) study(i interface{}) error {
	log.Printf("my name is %s, I am %d years old. I am a student, I study\n", p.Name, p.Age)
	return nil
}
func (p Person) teach(i interface{}) error {
	log.Printf("my name is %s, I am %d years old. I am a teacher, I teach\n", p.Name, p.Age)
	return nil
}
func (p Person) StudyFrom(t Teacher) error {
	log.Printf("my name is %s, I am %d years old. I am a student, I study from %s\n", p.Name, p.Age, t.(Person).Name)
	return nil
}
func (p Person) TeachTo(s Studyer) error {
	log.Printf("my name is %s, I am %d years old. I am a teacher, I teach to  %s\n", p.Name, p.Age, s.(Person).Name)
	return nil
}

func main() {
	I := Person{"michael", 28}
	a := Person{"peter", 44}
	b := Person{"jack", 60}
	c := Person{"gigi", 40}

	I.study(nil)

	a.teach(nil)
	a.study(nil)

	a.TeachTo(I)

	b.teach(nil)
	b.TeachTo(a)


	c.study(nil)
	c.StudyFrom(b)
}
